from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from backend.models.level import Level
import json


@csrf_exempt
def get_post(request):
    if request.method == 'GET':
        objects = Level.objects.all()
        queryset = serializers.serialize('json', objects)
        json_result = json.loads(queryset)

        return JsonResponse(json_result, status=200, safe=False)
    return "Only get methods allowed"

