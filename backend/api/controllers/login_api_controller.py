import base64

from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import AnonymousUser
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json


@csrf_exempt
def login(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return JsonResponse("LOGED", status=200, safe=False)
        if request.headers.get('Authorization'):
            auth = request.headers.get('Authorization').split()
            if len(auth) == 2:
                if auth[0].lower() == "basic":
                    username, password = str(base64.b64decode(auth[1])).replace('\'', "").split(':')
                    username = username[1:]
                    user = authenticate(username=username, password=password)
                    if user is not None:
                        auth_login(request, user)
                        return JsonResponse(str(user), safe=False, status=200)
        return JsonResponse("Please send valid user and password", safe=False, status=401)
    return JsonResponse("Only GET methods allowed", safe=False, status=400)


@csrf_exempt
def logout(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            auth_logout(request)
        request.session.flush()
        request.user = AnonymousUser()
        return JsonResponse("LOGOUT", safe=False, status=200)
    else:
        return JsonResponse("Only GET methods allowed", safe=False, status=400)
