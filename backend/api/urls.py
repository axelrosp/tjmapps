from django.conf.urls import url, include

from backend.api.controllers import level_api_controller, login_api_controller

urlpatterns = [
    url('level/', level_api_controller.get_post),
    url('login/', login_api_controller.login),
    url('logout/', login_api_controller.logout)
]
