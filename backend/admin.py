from django.contrib import admin

# Register your models here.
from backend.models.level import Level

admin.site.register(Level)