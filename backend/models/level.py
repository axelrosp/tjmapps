from django.db import models
from django.utils import timezone


class Level(models.Model):
    name = models.CharField(max_length=2)

    def publish(self):
        self.published_date = timezone.now()
        self.save()
